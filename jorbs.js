new Vue({
  el: "#scheduler",

  data: {
    commandToRun: "",
    dynoSize: "Free",
    jobFrequency: "Daily",
    lastRun: "never",
    nextDue: "00:00",
    now: Date().substr(4,6),
    commandToRunEdit: "",
    dynoSizeEdit: "Free",
    jobFrequencyEdit: "Daily",
    lastRunEdit: "never",
    nextDueEdit: "00:00",
    nowEdit: Date().substr(4,6),
    commandList: [],
    timeList: [],
    index: "",
  },

  methods: {
    addCommand: function() {
      var command = this.commandToRun.trim();
      var dyno = this.dynoSize;
      var frequency = this.jobFrequency;
      var run = this.lastRun;
      var due = this.nextDue;
      var today = this.now;

      if (command) {
        this.commandList.push({
          command, dyno, frequency, run, due, today
        });
        this.commandToRun = "";
        this.dynoSize = "Free";
        this.jobFrequency = "Daily";
        this.lastRun = "never";
        this.nextDue = "00:00";
      }
    },

    cancelCommand: function(command) {
        this.commandToRun = "";
        this.commandToRunEdit = "";
    },

    removeCommand: function(command) {
      this.index = this.commandList.indexOf(command);
      this.commandList.splice(this.index, 1);
      this.index = "";
    },

    indexCommand: function(command) {
      this.index = this.commandList.indexOf(command);
    },

    editCommand: function(command) {
      var commandEdit = this.commandToRunEdit.trim();
      var dynoEdit = this.dynoSizeEdit;
      var frequencyEdit = this.jobFrequencyEdit;
      var runEdit = this.lastRunEdit;
      var dueEdit = this.nextDueEdit;
      var todayEdit = this.nowEdit;


      if (commandEdit) {
        var command = commandEdit;
        var dyno = dynoEdit;
        var frequency = frequencyEdit;
        var run = runEdit;
        var due = dueEdit;
        var today = todayEdit;
        this.commandList.splice(this.index, 1, {command, dyno, frequency, run, due, today});
        this.index = "";
        this.commandToRunEdit = "";
        this.dynoSizeEdit = "Free";
        this.jobFrequencyEdit = "Daily";
        this.lastRunEdit = "never";
        this.nextDueEdit = "00:00";
      }
    },
  },

  computed: {
    halfHourList: function (){
      for(hour=0; hour<24; hour++) {
        for(minute=0; minute<2; minute++) {
          this.timeList.push((("00" + hour).substr(-2,2)) + ":" + (minute===0 ? "00" : 30*minute));
        }
      }
    }
  },
});